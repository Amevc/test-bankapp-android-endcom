package com.example.test_bankapp_android_endcom;

import android.app.Application;

import dagger.hilt.android.HiltAndroidApp;

@HiltAndroidApp
public final class BankApplication extends Application { }
