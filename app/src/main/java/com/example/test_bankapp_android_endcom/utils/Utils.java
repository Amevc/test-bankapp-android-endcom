package com.example.test_bankapp_android_endcom.utils;

import java.text.NumberFormat;
import java.util.Locale;

public class Utils {

    public static String formatearDouble(Double saldo){
        Locale mx = new Locale("es", "MX");
        NumberFormat dollarFormat = NumberFormat.getCurrencyInstance(mx);
        return dollarFormat.format(saldo);
    }

}
