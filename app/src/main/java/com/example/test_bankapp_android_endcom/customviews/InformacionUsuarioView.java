package com.example.test_bankapp_android_endcom.customviews;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.example.test_bankapp_android_endcom.R;
import com.example.test_bankapp_android_endcom.models.Usuario;


public class InformacionUsuarioView extends LinearLayout {

    private TextView nombre;
    private TextView ultimaSesion;

    public InformacionUsuarioView(Context context) {
        super(context);
        inflateView(context);
    }

    public InformacionUsuarioView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        inflateView(context);
    }

    public InformacionUsuarioView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        inflateView(context);
    }

    public InformacionUsuarioView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        inflateView(context);
    }

    private void inflateView(Context context){
        View view = LayoutInflater.from(context).inflate(R.layout.view_informacion_usuario, this, true);
        nombre = view.findViewById(R.id.nombre);
        ultimaSesion = view.findViewById(R.id.ultimaSesion);
    }

    public void colocarUsuario(Usuario usuario){
        nombre.setText(usuario.getNombre());
        ultimaSesion.setText(usuario.getUltimaSesion());
    }

}
