package com.example.test_bankapp_android_endcom.customviews;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;

import com.example.test_bankapp_android_endcom.R;
import com.example.test_bankapp_android_endcom.models.Movimiento;

import java.util.List;


public class ListaMovimientosView extends FrameLayout {

    private List<Movimiento> movimientos;
    private LinearLayout listaMovimientos;
    private Button mostrarMovimientos;
    private int movimientosMostrados;

    public ListaMovimientosView(Context context) {
        super(context);
        inflateView(context);
    }

    public ListaMovimientosView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        inflateView(context);
    }

    public ListaMovimientosView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        inflateView(context);
    }

    public ListaMovimientosView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        inflateView(context);
    }

    public void inflateView(Context context){
        View view = LayoutInflater.from(context).inflate(R.layout.view_lista_movimientos, this, true);
        listaMovimientos = view.findViewById(R.id.listaMovimientos);
        mostrarMovimientos = view.findViewById(R.id.mostrarMovimientos);
        mostrarMovimientos.setOnClickListener(v -> {
            mostrarTodos();
            mostrarMovimientos.setVisibility(INVISIBLE);
        });

    }

    public void colocarMovimientos(List<Movimiento> movimientos){
        this.movimientos = movimientos;
        pintarMovimientos();
    }

    private void pintarMovimientos(){
        movimientosMostrados = 3;
        if (movimientos.size() < 3)
            movimientosMostrados = movimientos.size();
        for (int i = 0; i < movimientosMostrados; i++){
            MovimientoView movimientoView = new MovimientoView(getContext());
            movimientoView.colocarMovimiento(movimientos.get(i));
            listaMovimientos.addView(movimientoView);
        }
    }

    private void mostrarTodos(){
        if (movimientosMostrados < movimientos.size()){
            for (int i = movimientosMostrados; i < movimientos.size(); i++ ){
                MovimientoView movimientoView = new MovimientoView(getContext());
                movimientoView.colocarMovimiento(movimientos.get(i));
                listaMovimientos.addView(movimientoView);
            }
            movimientosMostrados = movimientos.size();
        }
    }




}
