package com.example.test_bankapp_android_endcom.customviews;

import android.content.Context;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.util.AttributeSet;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class BankButton extends androidx.appcompat.widget.AppCompatTextView {


    public BankButton(@NonNull Context context) {
        super(context);
    }

    public BankButton(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public BankButton(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public void setText(CharSequence text, BufferType type) {
        SpannableString content = new SpannableString( text ) ;
        content.setSpan( new UnderlineSpan() , 0 , content.length() , 0 ) ;
        super.setText(content, type);
    }

}
