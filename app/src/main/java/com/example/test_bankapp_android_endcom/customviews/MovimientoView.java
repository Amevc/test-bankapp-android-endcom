package com.example.test_bankapp_android_endcom.customviews;

import android.content.Context;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;

import com.example.test_bankapp_android_endcom.R;
import com.example.test_bankapp_android_endcom.models.Movimiento;
import com.example.test_bankapp_android_endcom.utils.Utils;


public class MovimientoView extends ConstraintLayout {

    private TextView descripcion;
    private TextView fecha;
    private TextView monto;

    public MovimientoView(@NonNull Context context) {
        super(context);
        inflateView(context);
    }

    public MovimientoView(@NonNull Context context, @Nullable  AttributeSet attrs) {
        super(context, attrs);
        inflateView(context);
    }

    public MovimientoView(@NonNull Context context, @Nullable  AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        inflateView(context);
    }

    public MovimientoView(@NonNull  Context context, @Nullable  AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        inflateView(context);
    }

    private void inflateView(Context context){
        View view = LayoutInflater.from(context).inflate(R.layout.item_movimiento, this, true);
        descripcion = view.findViewById(R.id.descripcion);
        fecha = view.findViewById(R.id.fecha);
        monto = view.findViewById(R.id.monto);
    }

    public void colocarMovimiento(Movimiento movimiento){
        String descripcionCorta = movimiento.getDescripcion();
        if (movimiento.getDescripcion().length() > 12)
            descripcionCorta = movimiento.getDescripcion().substring(0,12) + "..";
        descripcion.setText(descripcionCorta);
        fecha.setText(movimiento.getFecha());
        Double montoDouble = Double.parseDouble(movimiento.getMonto());
        monto.setText(Utils.formatearDouble(montoDouble));
        if (movimiento.getTipo().equals("abono"))
            monto.setTextColor(Color.parseColor("#00FF00"));
        if (movimiento.getTipo().equals("cargo"))
            monto.setTextColor(Color.parseColor("#FF0000"));
    }





}
