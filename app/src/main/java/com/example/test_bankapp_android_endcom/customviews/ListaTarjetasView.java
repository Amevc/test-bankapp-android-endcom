package com.example.test_bankapp_android_endcom.customviews;

import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;

import com.example.test_bankapp_android_endcom.models.Tarjeta;

import java.util.List;

public class ListaTarjetasView extends LinearLayout {

    private List<Tarjeta> tarjetas;

    public ListaTarjetasView(Context context) {
        super(context);
    }

    public ListaTarjetasView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public ListaTarjetasView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public void colocarTarjetas(List<Tarjeta> tarjetas){
        setOrientation(VERTICAL);
        this.tarjetas = tarjetas;
        pintarTarjetas();
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    private void pintarTarjetas(){
        tarjetas.forEach(tarjeta -> {
            TarjetaView tarjetaView = new TarjetaView(getContext());
            tarjetaView.colocarTarjeta(tarjeta);
            addView(tarjetaView);
        });
    }

}
