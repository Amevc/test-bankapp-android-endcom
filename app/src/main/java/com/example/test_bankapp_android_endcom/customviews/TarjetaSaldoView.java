package com.example.test_bankapp_android_endcom.customviews;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;

import com.example.test_bankapp_android_endcom.R;
import com.example.test_bankapp_android_endcom.models.Saldo;
import com.example.test_bankapp_android_endcom.utils.Utils;


public class TarjetaSaldoView extends ConstraintLayout {

    private TextView titulo;
    private TextView dinero;

    public TarjetaSaldoView(@NonNull  Context context) {
        super(context);
        inflateView(context);
    }

    public TarjetaSaldoView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        inflateView(context);
    }

    public TarjetaSaldoView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        inflateView(context);
    }

    public TarjetaSaldoView(@NonNull  Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        inflateView(context);
    }

    private void inflateView(Context context){
        View view = LayoutInflater.from(context).inflate(R.layout.view_tarjeta_saldo, this, true);
        titulo = view.findViewById(R.id.titulo);
        dinero = view.findViewById(R.id.dinero);
    }

    public void colocarSaldo(Saldo saldo){
        titulo.setText(saldo.getTipo());
        dinero.setText(Utils.formatearDouble(saldo.getSaldo()));
    }

}
