package com.example.test_bankapp_android_endcom.customviews;

import android.content.Context;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;

import com.example.test_bankapp_android_endcom.R;
import com.example.test_bankapp_android_endcom.models.Tarjeta;

import static com.example.test_bankapp_android_endcom.utils.Utils.formatearDouble;


public class TarjetaView extends ConstraintLayout {

    private FrameLayout tarjetaImage;
    private TextView estado;
    private TextView nombre;
    private TextView saldo;
    private TextView tarjeta;
    private TextView tipo;

    public TarjetaView(Context context) {
        super(context);
        inflateView(context);
    }

    public TarjetaView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        inflateView(context);
    }

    public TarjetaView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        inflateView(context);
    }

    private void inflateView(Context context){
        View view = LayoutInflater.from(context).inflate(R.layout.item_tarjeta, this, true);
        tarjetaImage = view.findViewById(R.id.tarjetaImagen);
        estado = view.findViewById(R.id.estado);
        nombre = view.findViewById(R.id.nombre);
        saldo = view.findViewById(R.id.saldo);
        tarjeta = view.findViewById(R.id.tarjeta);
        tipo = view.findViewById(R.id.tipo);
    }

    public void colocarTarjeta(Tarjeta tarjeta){
        String estadoFormateado = tarjeta.getEstado().substring(0,1).toUpperCase()
                + tarjeta.getEstado().substring(1).toLowerCase();
        estado.setText(estadoFormateado);
        cambiarColorTarjeta();
        nombre.setText(tarjeta.getNombre());
        saldo.setText(formatearDouble(tarjeta.getSaldo()));
        this.tarjeta.setText(tarjeta.getTarjeta());
        String tipoFormateado = tarjeta.getTipo().substring(0,1).toUpperCase()
                + tarjeta.getTipo().substring(1).toLowerCase();
        tipo.setText(tipoFormateado);
    }



    private void cambiarColorTarjeta(){
        if (estado.getText().equals("Activa"))
            tarjetaImage.setBackgroundColor(Color.parseColor("#1FC49D"));
        else
            tarjetaImage.setBackgroundColor(Color.parseColor("#ABE5D6"));
    }

}

