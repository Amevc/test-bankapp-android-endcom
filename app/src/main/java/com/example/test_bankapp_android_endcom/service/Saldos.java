package com.example.test_bankapp_android_endcom.service;

import com.squareup.moshi.Json;

public class Saldos {

    @Json(name = "saldoGeneral")
    public Double saldoGeneral;
    @Json(name = "ingresos")
    public Double ingresos;
    @Json(name = "gastos")
    public Double gastos;

}
