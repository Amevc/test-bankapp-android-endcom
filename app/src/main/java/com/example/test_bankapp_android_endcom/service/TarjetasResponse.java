package com.example.test_bankapp_android_endcom.service;

import com.example.test_bankapp_android_endcom.models.Tarjeta;
import com.squareup.moshi.Json;

import java.util.List;

public class TarjetasResponse {

    @Json(name = "tarjetas")
    public List<Tarjeta> tarjetas;

}
