package com.example.test_bankapp_android_endcom.service;

import retrofit2.Call;
import retrofit2.http.GET;

public interface BankService {


    @GET("cuenta")
    Call<UsuarioResponse> conseguirUsuario();

    @GET("saldos")
    Call<SaldosResponse> conseguirSaldos();

    @GET("tarjetas")
    Call<TarjetasResponse> conseguirTarjetas();

    @GET("movimientos")
    Call<MovimientosResponse> conseguirMovimientos();

}
