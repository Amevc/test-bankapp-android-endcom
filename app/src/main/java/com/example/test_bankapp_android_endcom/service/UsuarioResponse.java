package com.example.test_bankapp_android_endcom.service;

import com.example.test_bankapp_android_endcom.models.Usuario;
import com.squareup.moshi.Json;

import java.util.List;

public class UsuarioResponse {

    @Json(name = "cuenta")
    public List<Usuario> cuenta;

}
