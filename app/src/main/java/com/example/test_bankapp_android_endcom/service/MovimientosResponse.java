package com.example.test_bankapp_android_endcom.service;

import com.example.test_bankapp_android_endcom.models.Movimiento;
import com.squareup.moshi.Json;

import java.util.List;

public class MovimientosResponse {

    @Json(name = "movimientos")
    public List<Movimiento> movimientos;

}
