package com.example.test_bankapp_android_endcom.service;

import com.squareup.moshi.Json;

import java.util.List;

public class SaldosResponse {

    @Json(name = "saldos")
    public List<Saldos> saldos;

}
