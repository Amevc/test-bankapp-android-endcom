package com.example.test_bankapp_android_endcom.models;

public class Form {

    private String numeroTarjeta;
    private String cuenta;
    private String issure;
    private String nombreTarjeta;
    private String marca;
    private String estatus;
    private String saldo;
    private String tipoCuenta;

    public Form(String numeroTarjeta, String cuenta, String issure, String nombreTarjeta, String marca, String estatus, String saldo, String tipoCuenta) {
        this.numeroTarjeta = numeroTarjeta;
        this.cuenta = cuenta;
        this.issure = issure;
        this.nombreTarjeta = nombreTarjeta;
        this.marca = marca;
        this.estatus = estatus;
        this.saldo = saldo;
        this.tipoCuenta = tipoCuenta;
    }

    public String getNumeroTarjeta() {
        return numeroTarjeta;
    }

    public void setNumeroTarjeta(String numeroTarjeta) {
        this.numeroTarjeta = numeroTarjeta;
    }

    public String getCuenta() {
        return cuenta;
    }

    public void setCuenta(String cuenta) {
        this.cuenta = cuenta;
    }

    public String getIssure() {
        return issure;
    }

    public void setIssure(String issure) {
        this.issure = issure;
    }

    public String getNombreTarjeta() {
        return nombreTarjeta;
    }

    public void setNombreTarjeta(String nombreTarjeta) {
        this.nombreTarjeta = nombreTarjeta;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getEstatus() {
        return estatus;
    }

    public void setEstatus(String estatus) {
        this.estatus = estatus;
    }

    public String getSaldo() {
        return saldo;
    }

    public void setSaldo(String saldo) {
        this.saldo = saldo;
    }

    public String getTipoCuenta() {
        return tipoCuenta;
    }

    public void setTipoCuenta(String tipoCuenta) {
        this.tipoCuenta = tipoCuenta;
    }
}
