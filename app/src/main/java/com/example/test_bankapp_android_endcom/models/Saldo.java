package com.example.test_bankapp_android_endcom.models;

public class Saldo {

    private String tipo;
    private Double saldo;

    public Saldo(String tipo, Double saldo) {
        this.tipo = tipo;
        this.saldo = saldo;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public Double getSaldo() {
        return saldo;
    }

    public void setSaldo(Double saldo) {
        this.saldo = saldo;
    }
}
