package com.example.test_bankapp_android_endcom.models;

import com.squareup.moshi.Json;

public class Tarjeta {

    @Json(name = "tarjeta")
    private String tarjeta;
    @Json(name = "nombre")
    private String nombre;
    @Json(name = "saldo")
    private Double saldo;
    @Json(name = "estado")
    private String estado;
    @Json(name = "tipo")
    private String tipo;

    public Tarjeta(String tarjeta, String nombre, Double saldo, String estado, String tipo) {
        this.tarjeta = tarjeta;
        this.nombre = nombre;
        this.saldo = saldo;
        this.estado = estado;
        this.tipo = tipo;
    }

    public String getTarjeta() {
        return tarjeta;
    }

    public void setTarjeta(String tarjeta) {
        this.tarjeta = tarjeta;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Double getSaldo() {
        return saldo;
    }

    public void setSaldo(Double saldo) {
        this.saldo = saldo;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

}
