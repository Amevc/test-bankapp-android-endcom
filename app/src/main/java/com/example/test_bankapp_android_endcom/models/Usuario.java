package com.example.test_bankapp_android_endcom.models;

import com.squareup.moshi.Json;

public class Usuario {

    @Json(name = "nombre")
    private String nombre;
    @Json(name = "ultimaSesion")
    private String ultimaSesion;

    public Usuario(String nombre, String ultimaSesion) {
        this.nombre = nombre;
        this.ultimaSesion = ultimaSesion.substring(0,9);
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getUltimaSesion() {
        return ultimaSesion;
    }

    public void setUltimaSesion(String ultimaSesion) {
        this.ultimaSesion = ultimaSesion;
    }
}
