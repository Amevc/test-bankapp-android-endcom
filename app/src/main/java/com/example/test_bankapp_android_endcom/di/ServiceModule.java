package com.example.test_bankapp_android_endcom.di;

import com.example.test_bankapp_android_endcom.service.BankService;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import dagger.hilt.InstallIn;
import dagger.hilt.components.SingletonComponent;
import retrofit2.Retrofit;
import retrofit2.converter.moshi.MoshiConverterFactory;

@Module
@InstallIn(SingletonComponent.class)
final class ServiceModule {

    @Provides
    @Singleton
    static BankService provideService(Retrofit.Builder builder) {
        return builder
                .build()
                .create(BankService.class);
    }

    @Provides
    static Retrofit.Builder provideRetrofit() {
        return new Retrofit.Builder()
                .baseUrl("http://bankapp.endcom.mx/api/bankappTest/")
                .addConverterFactory(MoshiConverterFactory.create());
    }

}
