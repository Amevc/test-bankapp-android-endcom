package com.example.test_bankapp_android_endcom.features.dashboard;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.test_bankapp_android_endcom.models.Movimiento;
import com.example.test_bankapp_android_endcom.models.Saldo;
import com.example.test_bankapp_android_endcom.models.Tarjeta;
import com.example.test_bankapp_android_endcom.models.Usuario;
import com.example.test_bankapp_android_endcom.service.BankService;
import com.example.test_bankapp_android_endcom.service.MovimientosResponse;
import com.example.test_bankapp_android_endcom.service.SaldosResponse;
import com.example.test_bankapp_android_endcom.service.TarjetasResponse;
import com.example.test_bankapp_android_endcom.service.UsuarioResponse;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import dagger.hilt.android.lifecycle.HiltViewModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@HiltViewModel
public class DashboardViewModel extends ViewModel {

    private String TAG = this.getClass().getSimpleName();

    private BankService bankService;

    public MutableLiveData<Usuario> usuario = new MutableLiveData<>();
    public MutableLiveData<ArrayList<Saldo>> saldos = new MutableLiveData<>();
    public MutableLiveData<List<Tarjeta>> tarjetas = new MutableLiveData<>();
    public MutableLiveData<List<Movimiento>> movimientos = new MutableLiveData<>();

    @Inject
    public DashboardViewModel(BankService bankService) {
        this.bankService = bankService;
        conseguirUsuario();
        conseguirSaldos();
        conseguirTarjetas();
        conseguirMovimientos();
    }


    private void conseguirUsuario(){
        bankService.conseguirUsuario().enqueue(new Callback<UsuarioResponse>() {
            @Override
            public void onResponse(Call<UsuarioResponse> call, Response<UsuarioResponse> response) {
                if (response.body() != null)
                    usuario.setValue(response.body().cuenta.get(0));
            }

            @Override
            public void onFailure(Call<UsuarioResponse> call, Throwable t) {
                Log.e(TAG, t.getMessage());
            }
        });
    }

    private void conseguirSaldos(){
        bankService.conseguirSaldos().enqueue(new Callback<SaldosResponse>() {
            @Override
            public void onResponse(Call<SaldosResponse> call, Response<SaldosResponse> response) {
                if (response.body() != null){

                    ArrayList<Saldo> lista = new ArrayList<>();
                    lista.add(new Saldo("Saldo general en cuentas", response.body().saldos.get(0).saldoGeneral));
                    lista.add(new Saldo("Total de ingresos", response.body().saldos.get(0).ingresos));
                    lista.add(new Saldo("Total de gastos", response.body().saldos.get(0).gastos));

                    saldos.setValue(lista);

                }
            }

            @Override
            public void onFailure(Call<SaldosResponse> call, Throwable t) {
                Log.e(TAG, t.getMessage());
            }
        });
    }

    private void conseguirTarjetas(){
        bankService.conseguirTarjetas().enqueue(new Callback<TarjetasResponse>() {
            @Override
            public void onResponse(Call<TarjetasResponse> call, Response<TarjetasResponse> response) {
                if (response.body() != null){
                    tarjetas.setValue(response.body().tarjetas);
                }
            }

            @Override
            public void onFailure(Call<TarjetasResponse> call, Throwable t) {
                Log.e(TAG, t.getMessage());
            }
        });
    }

    private void conseguirMovimientos(){
        bankService.conseguirMovimientos().enqueue(new Callback<MovimientosResponse>() {
            @Override
            public void onResponse(Call<MovimientosResponse> call, Response<MovimientosResponse> response) {
                if (response.body() != null){
                    movimientos.setValue(response.body().movimientos);
                }
            }

            @Override
            public void onFailure(Call<MovimientosResponse> call, Throwable t) {
                Log.e(TAG, t.getMessage());
            }
        });
    }



}
