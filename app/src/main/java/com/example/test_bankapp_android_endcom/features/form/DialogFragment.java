package com.example.test_bankapp_android_endcom.features.form;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.test_bankapp_android_endcom.R;

import dagger.hilt.android.AndroidEntryPoint;

@AndroidEntryPoint
public class DialogFragment extends androidx.fragment.app.DialogFragment {

    private Button aceptar;
    private TextView json;

    public DialogFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_dialog, container, false);

        aceptar = view.findViewById(R.id.aceptar);
        json = view.findViewById(R.id.json);

        json.setText(DialogFragmentArgs.fromBundle(getArguments()).getJsonString());

        aceptar.setOnClickListener(v -> {
            dismiss();
        });

        return view;
    }
}