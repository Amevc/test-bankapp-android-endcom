package com.example.test_bankapp_android_endcom.features.dashboard;

import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.test_bankapp_android_endcom.customviews.TarjetaSaldoView;
import com.example.test_bankapp_android_endcom.models.Saldo;

import java.util.ArrayList;

public class TarjetaSaldoAdapter extends RecyclerView.Adapter<TarjetaSaldoAdapter.TarjetaSaldoViewHolder>{

    private ArrayList<Saldo> saldosList;

    public TarjetaSaldoAdapter(ArrayList<Saldo> saldosList){
        this.saldosList = saldosList;
    }

    @NonNull
    @Override
    public TarjetaSaldoViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new TarjetaSaldoViewHolder(new TarjetaSaldoView(parent.getContext()));
    }

    @Override
    public void onBindViewHolder(@NonNull TarjetaSaldoViewHolder holder, int position) {
        holder.bind(saldosList.get(position));
    }

    @Override
    public int getItemCount() {
        return saldosList.size();
    }

    public static class TarjetaSaldoViewHolder extends RecyclerView.ViewHolder {

        private TarjetaSaldoView tarjetaSaldoView;

        public TarjetaSaldoViewHolder(TarjetaSaldoView itemView){
            super(itemView);
            tarjetaSaldoView = itemView;

        }

        public void bind(Saldo saldo){
            tarjetaSaldoView.colocarSaldo(saldo);
        }

    }

}
