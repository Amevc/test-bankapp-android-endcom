package com.example.test_bankapp_android_endcom.features.form;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.test_bankapp_android_endcom.R;
import com.example.test_bankapp_android_endcom.models.Form;
import com.google.android.material.textfield.TextInputLayout;
import com.squareup.moshi.Moshi;

import dagger.hilt.android.AndroidEntryPoint;


@AndroidEntryPoint
public class FormFragment extends Fragment {

    private String TAG = this.getClass().getSimpleName();

    private TextInputLayout numeroTarjeta;
    private TextInputLayout cuenta;
    private TextInputLayout issure;
    private TextInputLayout nombreTarjeta;
    private TextInputLayout marca;
    private TextInputLayout estatus;
    private TextInputLayout saldo;
    private TextInputLayout tipoCuenta;

    private Button agregar;

    public FormFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_form, container, false);

        findViews(view);

        agregar.setOnClickListener(v -> {
            FormFragmentDirections.ActionFormFragmentToDialogFragment action =
                    FormFragmentDirections.actionFormFragmentToDialogFragment(crearJson());
            Navigation.findNavController(v).navigate(action);
        });

        return view;
    }

    private void findViews(View v){
        numeroTarjeta = v.findViewById(R.id.numeroTarjeta);
        cuenta = v.findViewById(R.id.cuenta);
        issure = v.findViewById(R.id.issure);
        nombreTarjeta = v.findViewById(R.id.nombreTarjeta);
        marca = v.findViewById(R.id.marca);
        estatus = v.findViewById(R.id.estatus);
        saldo = v.findViewById(R.id.saldo);
        tipoCuenta = v.findViewById(R.id.tipoCuenta);

        agregar = v.findViewById(R.id.agregar);
    }

    private String crearJson(){
        String numeroTarjeta = this.numeroTarjeta.getEditText().getText().toString();
        String cuenta = this.cuenta.getEditText().getText().toString();
        String issure = this.issure.getEditText().getText().toString();
        String nombreTarjeta = this.nombreTarjeta.getEditText().getText().toString();
        String marca = this.marca.getEditText().getText().toString();
        String estatus = this.estatus.getEditText().getText().toString();
        String saldo = this.saldo.getEditText().getText().toString();
        String tipoCuenta = this.tipoCuenta.getEditText().getText().toString();
        Form form = new Form(numeroTarjeta, cuenta, issure, nombreTarjeta, marca, estatus, saldo, tipoCuenta);
        return new Moshi.Builder().build().adapter(Form.class).toJson(form);
    }



}