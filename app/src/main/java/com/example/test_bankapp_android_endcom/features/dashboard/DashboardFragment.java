package com.example.test_bankapp_android_endcom.features.dashboard;

import android.os.Build;
import android.os.Bundle;

import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.test_bankapp_android_endcom.R;
import com.example.test_bankapp_android_endcom.customviews.BankButton;
import com.example.test_bankapp_android_endcom.customviews.InformacionUsuarioView;
import com.example.test_bankapp_android_endcom.customviews.ListaMovimientosView;
import com.example.test_bankapp_android_endcom.customviews.ListaTarjetasView;

import dagger.hilt.android.AndroidEntryPoint;

@AndroidEntryPoint
public class DashboardFragment extends Fragment {

    private InformacionUsuarioView informacionUsuarioView;
    private RecyclerView tarjetasSaldoRecyclerView;
    private BankButton bankButton;
    private ListaTarjetasView listaTarjetasView;
    private ListaMovimientosView listaMovimientosView;

    private DashboardViewModel dashboardViewModel;

    public DashboardFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        dashboardViewModel = new ViewModelProvider(this).get(DashboardViewModel.class);

    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_dashboard, container, false);

        findViews(view);

        colocarInformacionUsuarioView();
        colocarTarjetasSaldo();
        colocarBoton();
        colocarListaTarjetasView();
        colocarListaMovimientos();



        return view;
    }

    private void findViews(View view){
        informacionUsuarioView = view.findViewById(R.id.informacionUsuario);
        tarjetasSaldoRecyclerView = view.findViewById(R.id.tarjetaSaldoRecyclerView);
        bankButton = view.findViewById(R.id.agregarTarjeta);
        listaTarjetasView = view.findViewById(R.id.listaTarjetasView);
        listaMovimientosView = view.findViewById(R.id.listaMovimientosView);
    }

    private void colocarInformacionUsuarioView(){
        dashboardViewModel.usuario.observe(getViewLifecycleOwner(), usuario -> {
            informacionUsuarioView.colocarUsuario(usuario);
        });
    }

    private void colocarTarjetasSaldo(){
        dashboardViewModel.saldos.observe(getViewLifecycleOwner(), saldos -> {
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL,
                    false);
            tarjetasSaldoRecyclerView.setLayoutManager(linearLayoutManager);
            TarjetaSaldoAdapter tarjetaSaldoAdapter = new TarjetaSaldoAdapter(saldos);
            tarjetasSaldoRecyclerView.setAdapter(tarjetaSaldoAdapter);
        });
    }

    private void colocarBoton(){
        bankButton.setOnClickListener(v -> {
            Navigation.findNavController(v).navigate(R.id.action_dashboardFragment_to_formFragment);
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    private void colocarListaTarjetasView(){
        dashboardViewModel.tarjetas.observe(getViewLifecycleOwner(), tarjetas -> {
            listaTarjetasView.colocarTarjetas(tarjetas);
        });
    }

    private void colocarListaMovimientos(){
        dashboardViewModel.movimientos.observe(getViewLifecycleOwner(), movimientos -> {
            listaMovimientosView.colocarMovimientos(movimientos);
        });
    }

}